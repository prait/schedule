<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header('Content-Type: application/json');
		
	if (trim($_SERVER['PHP_AUTH_USER'])=="Fr@nk2Gi" and trim($_SERVER['PHP_AUTH_PW']) == "FwdGi2@20") {

	include_once("../include/misc.php");
	$conn = connect_db();

	$data = json_decode(file_get_contents('php://input'), true);

	$policy_id 	= $data["policy_id"];

	if ($policy_id != "") {
	
	$sql = "SELECT * FROM MAS_APPLICATION";
	$sql .= " WHERE POLICY_ID = '" . $policy_id ."'";
	$result = mysql_query($sql, $conn);
	if ($rs = mysql_fetch_array($result)) {
					
		include("setPDF.php");
														
		$pdf->AddPage();
		//-------------------------------------------------------//
		$pdf->Image('images/FWD_logo_full_colour_CMYK.png', 5, 6, 30, 20);            
		$pdf->SetFont('cordiaupc', '', 10);
		$pdf->MultiCell(193, 10, "44/1 อ.รุ่งโรจน์ธนกุล ชั้น 12 ถ.รัชดาภิเษก  แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310 Tel: +66 (0) 2202 9500 Fax: +66 (0) 2202 9555", 0, 'L', 0, 0, 9, 20, true); 
		$pdf->MultiCell(193, 10, "ทะเบียนเลขที่  0107555000597", 0, 'R', 0, 0, 9, 20, true); 
		$pdf->MultiCell(193, 10, "44/1 12 Flr, Rungrojthanakul Bldg., Ratchadaphisek Rd., Huay Khwang,  Bangkok 10310 Tel: +66 (0) 2202 9500 Fax: +66 (0) 2202 9555", 0, 'L', 0, 0, 9, 23, true); 
		$pdf->MultiCell(193, 10, "เลขประจำตัวผู้เสียภาษี  0107555000597", 0, 'R', 0, 0, 9, 23, true); 

		$pdf->SetFont('angsanaupc', 'B', 16);
	  	$pdf->Text(70,20,'=== ตัวอย่างเพื่อทดสอบเท่านั้น ===');
		//$pdf->MultiCell(565, 35, "", 1, 'L', 0, 0, 15, 73, true); 
		$pdf->MultiCell(193, 10, "ตารางกรมธรรม์ประกันภัย", 0, 'C', 0, 0, 9, 28, true); 
		$pdf->MultiCell(193, 10, "กรมธรรม์ประกันภัยอุบัติเหตุและโรคติดเชื้อไวรัสโคโรนา 2019 (COVID-19)", 0, 'C', 0, 0, 9, 33, true); 
		$pdf->MultiCell(193, 10, "(สำหรับการขายผ่านทางอิเล็กทรอนิกส์ (Online))", 0, 'C', 0, 0, 9, 38, true); 
		//$pdf->MultiCell(193, 10, "ตารางกรมธรรม์ประกันภัย", 0, 'C', 0, 0, 9, 30, true); 
		//$pdf->MultiCell(193, 10, "กรมธรรม์ประกันภัยอุบัติเหตุและโรคติดเชื้อไวรัสโคโรนา 2019 (COVID-19)", 0, 'C', 0, 0, 9, 36, true); 

		$pdf->MultiCell(95, 8, "SCI", 0, 'L', 0, 0, 35, 48, true); 

		$pdf->SetFont('angsanaupc', 'N', 16);
		$pdf->MultiCell(193, 8, "", 1, 'L', 0, 0, 9, 48, true); 
		$pdf->MultiCell(95, 8, "รหัสบริษัท :", 0, 'L', 0, 0, 9, 48, true); 
		$pdf->MultiCell(98, 8, "กรมธรรม์ประกันภัยเลขที่ :", 0, 'L', 0, 0, 100, 48, true); 
		$pdf->MultiCell(98, 8, $rs['POLICY_ID'], 0, 'R', 0, 0, 100, 48, true); 

		$pdf->MultiCell(193, 24, "", 1, 'L', 0, 0, 9, 57, true); 
		$pdf->MultiCell(193, 10, "1. ชื่อผู้เอาประกันภัย :", 0, 'L', 0, 0, 9, 57, true); 
		$pdf->MultiCell(193, 10, "ที่อยู่ปัจจุบัน :", 0, 'L', 0, 0, 13, 65, true); 
		$pdf->MultiCell(193, 10, $rs['tempTitleLifeNm'].$rs['tempFirstLifeNm']." ".$rs['tempLastLifeNm'], 0, 'L', 0, 0, 45, 56, true); 
		$pdf->SetFont('angsanaupc', 'N', 14);
		$pdf->MultiCell(193, 10, "เลขที่ ".$rs['tempLifeAddrNo']." ".$rs['tempLifeBuilding'], 0, 'L', 0, 0, 45, 63, true); 
		$pdf->MultiCell(193, 10, $rs['tempLifeTambol']." ".$rs['tempLifeAmphur']." ".$rs['tempLifeChangewat']." รหัสไปรษณีย์ ".$rs['tempLifePostcode'], 0, 'L', 0, 0, 45, 68, true); 
		//$pdf->MultiCell(193, 10, $rs['tempLifeTambol']." ".$rs['tempLifeAmphur']." ".$rs['tempLifeChangewat']." รหัสไปรษณีย์ ".$rs['tempLifePostcode'], 0, 'L', 0, 0, 13, 67, true); 
		//$pdf->MultiCell(60, 24, $rs['tempLifeIc'], 0, 'R', 0, 0, 140, 52, true); 

		$POLICY_BDATE	= substr($rs["tempLifeBirth"], -4)."-".substr($rs["tempLifeBirth"],3,2)."-".substr($rs["tempLifeBirth"],0,2);
		$age = date("Y", strtotime($rs["POLICY_TRANDATE"])) - date("Y", strtotime($POLICY_BDATE));

		$pdf->SetFont('angsanaupc', 'N', 16);
		$pdf->MultiCell(100, 10, "เพศ :", 0, 'L', 0, 0, 13, 73, true); 
		$pdf->MultiCell(100, 10, "วัน-เดือน-ปีเกิด : ", 0, 'L', 0, 0, 70, 73, true); 
		$pdf->MultiCell(90, 10, "อาชีพ :", 0, 'L', 0, 0, 150, 73, true); 
		$pdf->MultiCell(90, 10, "-", 0, 'L', 0, 0, 170, 73, true); 
		$pdf->MultiCell(100, 10, $GENDER, 0, 'L', 0, 0, 25, 73, true); 
		$pdf->MultiCell(100, 10, PrintDate($POLICY_BDATE), 0, 'L', 0, 0, 100, 73, true); 
		//$pdf->MultiCell(60, 24, $age."   ปี", 0, 'L', 0, 0, 155, 67, true); 
				
		$pdf->MultiCell(193, 18, "", 1, 'L', 0, 0, 9, 81, true); 
		$pdf->MultiCell(95, 10, "2. ชื่อผู้รับประโยชน์ :", 0, 'L', 0, 0, 9, 81, true); 
		$pdf->MultiCell(95, 10, "ความสัมพันธ์กับผู้เอาประกันภัย :", 0, 'L', 0, 0, 100, 81, true); 
		$pdf->MultiCell(95, 10, "ที่อยู่ปัจจุบัน :", 0, 'L', 0, 0, 13, 90, true); 
		$pdf->MultiCell(95, 10, $rs['tempTitleBeneNm'].$rs['tempFirstBeneNm']." ".$rs['tempLastBeneNm'], 0, 'L', 0, 0, 45, 81, true); 
		$pdf->MultiCell(95, 10, $rs['tempRelation'], 0, 'L', 0, 0, 155, 81, true); 

		$pdf->MultiCell(193, 10, "", 1, 'L', 0, 0, 9, 99, true); 
		$pdf->MultiCell(58, 10, "3. ระยะเวลาเอาประกันภัย : เริ่มวันที่", 0, 'L', 0, 0, 9, 99, true);
		$pdf->MultiCell(25, 10, "สิ้นสุดวันที่", 0, 'L', 0, 0, 125, 99, true); 
		$pdf->MultiCell(28, 10, "เวลา 24.00 น.", 0, 'R', 0, 0, 174, 99, true); 

		$pdf->MultiCell(40, 10, PrintDate($rs['POLICY_COMDATE']), 0, 'C', 0, 0, 60, 99, true);
		if($rs['POLICY_COMDATE'] > $rs['POLICY_TRANDATE']) {
			$pdf->MultiCell(25, 10, "เวลา 00.01 น.", 0, 'R', 0, 0, 100, 99, true);
		} else {
			$pdf->MultiCell(25, 10, "เวลา ".substr($rs['POLICY_TRANDATE'],11,5)." น.", 0, 'R', 0, 0, 100, 99, true);
		}
		$pdf->MultiCell(40, 10, PrintDate($rs['POLICY_EXPDATE']), 0, 'C', 0, 0, 140, 99, true); 

		$pdf->MultiCell(193, 10, "", 1, 'L', 0, 0, 9, 109, true); 
		$pdf->MultiCell(193, 10, "4. จำนวนจำกัดความรับผิด : กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะข้อที่มีจำนวนเงินผลประโยชน์ระบุไว้เท่านั้น", 0, 'L', 0, 0, 9, 109, true); 

		$pdf->MultiCell(133, 10,"", 1, 'C', 0, 0, 9, 119, true); 
		$pdf->MultiCell(30, 10, "", 1, 'C', 0, 0, 142, 119, true); 
		$pdf->MultiCell(30, 10, "", 1, 'C', 0, 0, 172, 119, true);

		$pdf->MultiCell(133, 10,"ข้อตกลงคุ้มครอง/เอกสารแนบท้าย", 0, 'C', 0, 0, 9, 119, true); 

		$pdf->MultiCell(133, 56, "", 1, 'C', 0, 0, 9, 129, true); 
		$pdf->MultiCell(30, 56, "", 1, 'C', 0, 0, 142, 129, true); 
		$pdf->MultiCell(30, 56, "", 1, 'C', 0, 0, 172, 129, true);

		$pdf->SetFont('angsanaupc', 'N', 12);
		$pdf->MultiCell(30, 10, "จำนวนเงินเอาประกันภัย", 0, 'C', 0, 0, 142, 119, true); 
		$pdf->MultiCell(30, 10, "(บาท)", 0, 'C', 0, 0, 142, 122, true); 
		$pdf->MultiCell(30, 10, "เบี้ยประกันภัย", 0, 'C', 0, 0, 172, 119, true);
		$pdf->MultiCell(30, 10, "(บาท)", 0, 'C', 0, 0, 172, 122, true);
					
		$sql_cover = "SELECT A.*, B.COVERAGE_NAME FROM scil.TXN_PACKAGE_COVERAGE A LEFT JOIN scil.LTB_COVERAGE B";
		$sql_cover .= " ON A.CID = B.CID";
		$sql_cover .= " WHERE A.PKG_ID = '".$rs['tempFormula']."' ";
//		$sql_cover .= " AND B.COVER_TYPE = '1' ";
		$sql_cover .= " AND (B.COVER_TYPE = '1' or B.COVER_TYPE = '2')";
		$sql_cover .= " ORDER BY A.CID";
		//echo $sql;
		$query_cover = mysql_query($sql_cover) or die (mysql_error());
		$numrows_cover = mysql_num_rows($query_cover);

		$sety = 128;					
		$pdf->SetFont('angsanaupc', 'N', 12);
		$pdf->SetXY(10,$sety);
							
		for($j=1; $j<=$numrows_cover; $j++) { // loop
		$row_cover = mysql_fetch_array($query_cover);

		$package_id = $row_cover["PKG_ID"];
		$cover =  $row_cover["COVERAGE_NAME"];
		$sum_insure =  $row_cover["COVERAGE"];
		$premium =  $row_cover["COVERAGE_PREM"];

			if(substr($package_id,0,3) == "COO") {
				$pdf->MultiCell(160, 16," - ".$cover, 0, 'L', 0, 0, 10, $sety, true);
				$pdf->MultiCell(30, 16,number_format($sum_insure,0), 0, 'R', 0, 0, 142, $sety, true);
				$pdf->MultiCell(30, 16,number_format($premium,2), 0, 'R', 0, 0, 172, $sety, true);
				$pdf->Ln();
				$sety = $sety+6;
				$pdf->SetXY(10,$sety);
			} 
							
		}

		$sql_package = "SELECT * FROM scil.MAS_PACKAGE2";
		$sql_package .= " WHERE PKG_ID = '".$rs['tempFormula']."' ";
		$result_package = mysql_query($sql_package, $conn);
		if ($rs_package = mysql_fetch_array($result_package)) {
			$pkg_name = $rs_package["PKG_NAME"];
			$price = $rs_package["NET_PREM"];
			$stamp = $rs_package["STAMP"];
			$tax = ($price+$stamp)*($rs_package["TAX"]/100);
			$prem_add = $rs_package["ADD_PREM"];
			$prem_disc = $rs_package["DISC_PREM"];
			$total_premium = $price+$stamp+$tax;
		}

		$pdf->SetFont('angsanaupc', 'N', 16);
		$pdf->MultiCell(163, 25, "", 1, 'L', 0, 0, 9, 185, true); 
		$pdf->MultiCell(163, 10, "เบี้ยประกันภัยสุทธิ   ", 0, 'R', 0, 0, 9, 185, true); 
		$pdf->MultiCell(163, 10, "อากรแสตมป์   ", 0, 'R', 0, 0, 9, 191, true); 
		$pdf->MultiCell(163, 10, "ภาษี   ", 0, 'R', 0, 0, 9, 196, true); 
		$pdf->MultiCell(163, 10, "เบี้ยประกันภัยรวม   ", 0, 'R', 0, 0, 9, 201, true); 
		$pdf->MultiCell(30, 25, "", 1, 'L', 0, 0, 172, 185, true); 
		$pdf->MultiCell(30, 10, number_format($price,2)." ", 0, 'R', 0, 0, 172, 185, true); 
		$pdf->MultiCell(30, 10, number_format($stamp,2)." ", 0, 'R', 0, 0, 172, 191, true); 
		$pdf->MultiCell(30, 10, number_format($tax,2)." ", 0, 'R', 0, 0, 172, 196, true); 
		$pdf->MultiCell(30, 10, number_format($total_premium,2), 0, 'R', 0, 0, 172, 201, true); 

		$pdf->MultiCell(193, 16, "", 1, 'L', 0, 0, 9, 210, true); 
		$pdf->Image('images/checkbox-square.png', 13, 213, 5, 5); 
		$pdf->MultiCell(40, 10, "การประกันภัยโดยตรง", 0, 'L', 0, 0, 18, 210, true); 
		$pdf->Image('images/checkbox-square.png', 55, 213, 5, 5); 
		$pdf->MultiCell(120, 10, "ตัวแทนประกันวินาศภัย", 0, 'L', 0, 0, 60, 210, true); 
		$pdf->Text(101,217,'X');
		$pdf->Image('images/checkbox-square.png', 100, 213, 5, 5); 
		$pdf->MultiCell(120, 10, "นายหน้าประกันวินาศภัย", 0, 'L', 0, 0, 105, 210, true); 
		$pdf->MultiCell(150, 10, "ใบอนุญาตเลขที่", 0, 'L', 0, 0, 144, 210, true); 
		
		$pdf->MultiCell(200, 10, "บริษัท โบลท์เทค อินชัวรันส์ โบรคเกอร์ (ประเทศไทย) จำกัด", 0, 'L', 0, 0, 105, 217, true); 
		$pdf->MultiCell(35, 10, "ว00017/2559", 0, 'R', 0, 0, 165, 210, true); 

		$pdf->MultiCell(193, 10, "", 1, 'L', 0, 0, 9, 226, true); 
		$pdf->MultiCell(95, 10, "วันทำสัญญาประกันภัย", 0, 'L', 0, 0, 9, 226, true);
		$tmp01 = PrintDate($rs['POLICY_TRANDATE']);
		$pdf->MultiCell(95, 10, $tmp01, 0, 'L', 0, 0, 50, 226, true); 
		$pdf->MultiCell(95, 10, "วันออกกรมธรรม์ประกันภัย", 0, 'L', 0, 0, 100, 226, true);
		$pdf->MultiCell(95, 10, $tmp01, 0, 'L', 0, 0, 150, 226, true); 

		$pdf->MultiCell(193, 40, "", 1, 'L', 0, 0, 9, 236, true); 
		$pdf->MultiCell(193, 10, "เพื่อเป็นหลักฐาน บริษัทโดยบุคคลผู้มีอำนาจได้ลงลายมือชื่อและประทับตราของบริษัทไว้เป็นสำคัญ ณ สำนักงานของบริษัท", 0, 'L', 0, 0, 9, 236, true); 
		//$pdf->MultiCell(193, 10, "As evidence, the Company has caused this policy to be signed by duly authorized persons and the Company's stamp to be affixed at Its office.", 0, 'L', 0, 0, 15, 700, true); 
		////////////--------------ลายเซ็น---------------------////////////////

		// -- ลายเซ็นต์ ---//
		$pdf->Image('images/kulawat.jpg', 19, 250, 27, 15);
		$pdf->Image('images/BOB.jpg', 90, 252, 25, 19);
		$pdf->Image('images/BOB.jpg', 155, 252, 25, 19);

		$pdf->Image('images/SEAL_FWDGI.png', 62, 252, 17, 17);

		$pdf->MultiCell(50, 10, "-------------------------------------", 0, 'C', 0, 0, 9, 260, true);
		$pdf->MultiCell(50, 10, "กรรมการ", 0, 'C', 0, 0, 9, 265, true);
		//$pdf->MultiCell(190, 10, "Director", 0, 'C', 0, 0, 15, 773, true);
		
		$pdf->MultiCell(50, 10, "-------------------------------------", 0, 'C', 0, 0, 80, 260, true);
		$pdf->MultiCell(50, 10, "กรรมการ", 0, 'C', 0, 0, 80, 265, true);
		//$pdf->MultiCell(193, 10, "Director", 0, 'C', 0, 0, 205, 773, true);
		
		$pdf->MultiCell(50, 10, "-------------------------------------", 0, 'C', 0, 0, 140, 260, true);
		$pdf->MultiCell(50, 10, "ผู้รับมอบอำนาจ", 0, 'C', 0, 0, 140, 265, true);
		//$pdf->MultiCell(193, 10, "Authorized Signature", 0, 'C', 0, 0, 395, 773, true);

		////////////--------------ลายเซ็น---------------------////////////////

		/*===========  PRINT CARE CARD =================*/
		if(substr($package_id,0,5) == "COO_V") {
			$pdf->AddPage();

			$pdf->Image('images/Care_Card_TPA.jpg', 45, 30, 114, 163);    
					
			$pdf->Text(70,17,'=== ตัวอย่างเพื่อทดสอบเท่านั้น ===');
			$pdf->SetFont('cordiaupc', '', 10);
			$pdf->MultiCell(190, 10, "กรมธรรม์เลขที่", 0, 'L', 0, 0, 65, 71, true);
			$pdf->MultiCell(190, 10, "บัตรประจำตัวประชาชนเลขที่", 0, 'L', 0, 0, 65, 75, true);
			$pdf->MultiCell(190, 10, "ชื่อผู้เอาประกัน :", 0, 'L', 0, 0, 65, 79, true);

			$pdf->MultiCell(190, 10, "ค่ารักษาพยาบาลจาก", 0, 'L', 0, 0, 65, 83, true);
			$pdf->MultiCell(190, 10, "-โรคติดเชื้อ COVID-19", 0, 'L', 0, 0, 70, 87, true);
			$pdf->MultiCell(190, 10, "-ผลข้างเคียงวัคซีน COVID-19", 0, 'L', 0, 0, 70, 91, true);

			$pdf->MultiCell(190, 10, "วันที่มีผลบังคับ", 0, 'L', 0, 0, 65, 95, true);
			$pdf->MultiCell(190, 10, "วันที่สิ้นสุด", 0, 'L', 0, 0, 105, 95, true);
			$pdf->MultiCell(190, 10, "ชื่อแผนประกันภัย", 0, 'L', 0, 0, 65, 99, true);

			$pdf->SetFont('cordiaupc', 'B', 10);
			$pdf->MultiCell(190, 10, $rs['POLICY_ID'], 0, 'L', 0, 0, 97, 71, true); 
			$pdf->MultiCell(190, 10, $rs['tempLifeIc'], 0, 'L', 0, 0, 97, 75, true); 
			$pdf->MultiCell(190, 10, $rs['tempTitleLifeNm'].$rs['tempFirstLifeNm']." ".$rs['tempLastLifeNm'], 0, 'L', 0, 0, 97, 79, true);
			if($package_id == "COO_V3A") {
				$pdf->MultiCell(50, 10, number_format("20000",0)." บาท", 0, 'R', 0, 0, 70, 87, true);
				$pdf->MultiCell(50, 10, number_format("10000",0)." บาท", 0, 'R', 0, 0, 70, 91, true);
			} else if($package_id == "COO_V5B") {
				$pdf->MultiCell(50, 10, number_format("50000",0)." บาท", 0, 'R', 0, 0, 70, 87, true);
				$pdf->MultiCell(50, 10, number_format("10000",0)." บาท", 0, 'R', 0, 0, 70, 91, true);
			}
			$pdf->MultiCell(190, 10, PrintDate($rs['POLICY_COMDATE']), 0, 'L', 0, 0, 80, 95, true);
			$pdf->MultiCell(190, 10, PrintDate($rs['POLICY_EXPDATE']), 0, 'L', 0, 0, 116, 95, true);
			$pdf->MultiCell(190, 10, $pkg_name, 0, 'L', 0, 0, 97, 99, true);

			$pdf->AddPage();
			$pdf->Image('images/TPA_Hospital_1.jpg', 5, 15, 198, 270); 
		}
		
		$pdf->lastPage();
		$datenow  = date('Ymd');          
		$path = "download/Data_".$datenow;			
							
		if(is_dir($path) === false){
			mkdir($path,0755, true);
		}				
							
		$pdf->Output($path."/".$policy_id.'.pdf','F');   
				
		$sql_log = "SELECT SENT_NO FROM TXN_LOG_SOAP";
		$sql_log .= " WHERE POLICY_ID = '" . $policy_id ."'";
		$sql_log .= " ORDER BY SENT_NO DESC";
		$result_log = mysql_query($sql_log);
		if ($rs_log = mysql_fetch_array($result_log)) {
			$sent_no = strval(intval($rs_log["SENT_NO"]) + 1);
		} else {
			$sent_no = '1';
		}

		$res = "Success:Send Email Successfully";
		
		$sql_log = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
		$sql_log .= " ('".$policy_id."', ". $sent_no .", 'EMAIL', now()";
		$sql_log .= ", '".getenv("REMOTE_ADDR")."', '1', '" . $res . "', '')";
		mysql_unbuffered_query($sql_log);

		$return  = array(
							"STATUS"=>"Success",
							"MESSAGE"=>"Successfully",
							);
								
		$res		= json_encode($return);
 
		// ------ POLICY ORIGINAL Siam PA -------//               
		
		$startdate = PrintDate($rs['POLICY_COMDATE']);
		$enddate = PrintDate($rs['POLICY_EXPDATE']);
		$tmp01 = PrintDate($rs['POLICY_TRANDATE']);

		// ------- /SEND EMAIL TO CUSTOMER ------- //

		$from 		= "fwdgi.web@fwdgi.co.th";
		$to 			= "fwdcovid.staging.th@edirectinsure.com,phumphat.son@fwdgi.com"; //UAT
		//$to 			= "fwdcovid.th@edirectinsure.com"; //PROD
		//$to 			= $rs['emailAddr']; 
		$subject 	= "FWDGI-".$policy_id;	
		$msg 		= "<table width='50%' align='center' border='0'>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/BN_1.png' width='947' height='84'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td width='10% colspan='5'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td colspan='3'><strong><font color='#e87722' face='Tahoma' size='4'>เรียน คุณ".$rs['tempFirstLifeNm']."    ".$rs['tempLastLifeNm']."</font></strong></td>
								<td width='5%'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td colspan='3'><font color='#183028' face='Tahoma' size='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บมจ. เอฟดับบลิวดีประกันภัย ได้รับข้อมูลการชำระค่าเบี้ยประกันของท่านเรียบร้อยแล้ว เมื่อวันที่ ".$tmp01." เวลา ".substr($rs['POLICY_TRANDATE'],11,5)." น. เพื่อยืนยันความคุ้มครอง บริษัทฯ ได้แนบหน้าตารางกรมธรรม์ อิเล็กทรอนิกส์มาให้ท่านตามอีเมลนี้</font></td>
								<td width='5%'></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><strong><font color='#183028' face='Tahoma' size='5'>เลขกรมธรรม์ของคุณคือ</font></strong></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><strong><font color='#e87722' face='Tahoma' size='6'>".$policy_id."</font></strong></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td height='10' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' width='5%'><img src='https://www.siamcityinsurance.com:9843/schedule/images/iconPaper.png' width='20' height='19'></td>
								<td align='left' colspan='4'><strong><font color='#e87722' face='Tahoma' size='4'>รายละเอียดกรมธรรม์"."</font></strong></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>กรมธรรม์ประเภท : "."</font></strong><font color='#183028' face='Tahoma' size='3'>กรมธรรม์ประกันภัยอุบัติเหตุและโรคติดเชื้อไวรัสโคโรนา 2019"."</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>วันที่เริ่มคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$startdate."</font></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>แผนตวามคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempFormula']."</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>วันที่สิ้นสุดความคุ้มครอง : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$enddate."</font></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>ราคา : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".number_format($total_premium,2)." บาท</font></td>
								<td colspan='2'><strong><font color='#183028' face='Tahoma' size='3'>บริษัท : "."</font></strong><font color='#183028' face='Tahoma' size='3'>เอฟดับบลิวดีประกันภัย จำกัด (มหาชน)"."</font></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' width='5%'><img src='https://www.siamcityinsurance.com:9843/schedule/images/iconPerson.png' width='20' height='22'></td>
								<td align='left' colspan='4'><strong><font color='#e87722' face='Tahoma' size='4'>รายละเอียดผู้เอาประกัน"."</font></strong></td>
							</tr>
							<tr>
								<td></td>
								<td colspan='4'><strong><font color='#183028' face='Tahoma' size='3'>ชื่อ : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempFirstLifeNm']."    ".$rs['tempLastLifeNm']."</font></td>
							<tr>
							   <td></td>
								<td colspan='4'><strong><font color='#183028' face='Tahoma' size='3'>เบอร์โทรศัพท์ : "."</font></strong><font color='#183028' face='Tahoma' size='3'>".$rs['tempLifeMobile']."</font></td>
							</tr>
								<td height='10' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/Line.jpg' width='947' height='5'/ alt='Line'></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><font color='#183028' face='Tahoma' size='3'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลโดยอัตโนมัติโดย บมจ. เอฟดับบลิวดีประกันภัย กรุณาอย่าตอบกลับ"."</font></td>
							</tr>
							<tr>
								<td height='15' colspan='5'></td>
							</tr>
							<tr>
								<td width='5%'></td>
								<td width='30%' align='center'></td>
								<td width='30%' align='center'></td>
								<td width='30%' align='center'></td>
								<td  width='5%'></td>
							</tr>
							<tr>
								<td align='center' colspan='5'><img src='https://www.siamcityinsurance.com:9843/schedule/images/BN_2.png' width='947' height='43'></td>
							</tr>
							</table>";
				
		$attach1 =$path."/".$policy_id.".pdf";
		//$attach2 ="CTP_Conditions_Exceptions.pdf";
							
		//$arrFiles = array($policy_filename,"$attach1","$attach2");
		$arrFiles = array($policy_filename,"$attach1");
						
		SendMail_MultiAttach($from, $to, $subject, $msg, $arrFiles);
		//SendMail_MultiAttach($from,'phumphat.son@fwdgi.com', $subject, $msg, $arrFiles);
		//SendMail_MultiAttach($from,'pannita.san@bolttech.io', $subject, $msg, $arrFiles);
		//SendMail_MultiAttach($from,'tiago.alves@edirectinsure.com', $subject, $msg, $arrFiles);
		
		mysql_free_result($result_log);
		mysql_close($conn);
		
		} else {
						
		$sql_log = "SELECT SENT_NO FROM TXN_LOG_SOAP";
		$sql_log .= " WHERE POLICY_ID = '" . $policy_id ."'";
		$sql_log .= " ORDER BY SENT_NO DESC";
		$result_log = mysql_query($sql_log);
		if ($rs_log = mysql_fetch_array($result_log)) {
			$sent_no = strval(intval($rs_log["SENT_NO"]) + 1);
		} else {
			$sent_no = '1';
		}

		$res = "Error:Data Not Found";
		
		$sql_log = "INSERT INTO TXN_LOG_SOAP (POLICY_ID, SENT_NO, SENT_TYPE, LOG_DATE, IP_ADDRESS, STATUS, MESSAGE, NOTE) VALUES";
		$sql_log .= " ('".$policy_id."', ". $sent_no .", 'EMAIL', now()";
		$sql_log .= ", '".getenv("REMOTE_ADDR")."', '0', '" . $res . "', '')";
		mysql_unbuffered_query($sql);
				
		$return  = array(
							"STATUS"=>"Error",
							"MESSAGE"=>"Data Not Found",
							);
										
						$res		= json_encode($return);
			
		mysql_free_result($result_log);
		mysql_close($conn);
		}
	} else {

		$return  = array(
							"STATUS"=>"Error",
							"MESSAGE"=>"Data Not Found",
							);
						
		$res		= json_encode($return);

	}
} else {

$return  = array(
					"STATUS"=>"Fail",
					"MESSAGE"=>"user & password  incorrect",
					);
				
$res	= json_encode($return);
}
echo $res;
return;

?>